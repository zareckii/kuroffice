﻿using System;
namespace KurOffice.ViewModel
{
    public class SaveBookingVM
    {
		public int officeId { get; set; }
		public int managerId { get; set; }
		public DateTime bookingDate { get; set; }
		public int clientId { get; set; }
		public decimal price { get; set; }
		public int term { get; set; }
		public string name { get; set; }
    }
}
