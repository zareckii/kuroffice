﻿using System;
namespace KurOffice.ViewModel
{
    public class ClientVM
    {
		public int ClientID { get; set; }
		public string FullName { get; set; }
		public string PhoneNumber { get; set; }
    }
}
