﻿using System;
namespace KurOffice.ViewModel
{
    public class BookingVM
    {
		public int BookingId { get; set; }
		public int ClientId { get; set; }
		public string ClientName { get; set; }
		public int OfficeId { get; set; }
		public string BookingName { get; set; }
		public int ManagerId { get; set; }
		public int? OfficeNumber { get; set; }
		public string ManagerName { get; set; }
		public decimal Price { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime CreatedDate { get; set; }
		public int? Term { get; set; }
		public bool isExpired { get; set; }
    }
}
