﻿using System;
using System.Numerics;

namespace KurOffice.Models
{
    public class OfficeStatisticVM
    {
		public int? office_id { get; set; }
		public int? OfficeNumber { get; set; }
		public int? ClientCount { get; set; }
		public decimal Price { get; set; }
		public int? AvgTerms { get; set; }
    }
}
