﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KurOffice.ViewModel
{
    public class OfficeVM
    {
		public int OfficesId { get; set; }
        public int? Area { get; set; }
        public int? Countroom { get; set; }
        public int? Floor { get; set; }
        public int? Number { get; set; }
        public bool? Kitchen { get; set; }
        public bool? Restroom { get; set; }
        public decimal Startprice { get; set; }
    }
}
