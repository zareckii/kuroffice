﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace KurOffice.ViewModel
{
    public class RigisterModel
    {
		[Required(ErrorMessage = "Не указан Login")]
		public string Login { get; set; }
		[Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
		public string Password { get; set; }
		[DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
		public string ConfirmPassword { get; set; }
    }
}
