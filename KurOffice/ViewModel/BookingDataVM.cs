﻿using System;
namespace KurOffice.ViewModel
{
    public class BookingDataVM
    {
        public DateTime Bookingdate { get; set; }
        public DateTime Startdate { get; set; }
        public int? Terms { get; set; }
    }
}
