﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using KurOffice.Models;
using KurOffice.ViewModel;
using Microsoft.AspNetCore.Authorization;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KurOffice.Controllers
{
	[Authorize]
	public class ManagerController : BaseController
    {
		private ManagersModel managersModel = new ManagersModel();

        // GET: /<controller>/
		public IActionResult Index()
        {
			var model = managersModel.ReturnAll().Select(x => new ManagerVM
            {
				ManagerID = x.ManagersId,
                FullName = x.Fullname,
                PhoneNumber = x.Phonenumber
            }).ToList();

            return View(model);
        }

        [HttpPost]
		public void AddManager(Managers item)
        {
			managersModel.Add(item);
        }

        [HttpPost]
        public ActionResult RemoveManager(int Id)
        {
            string msg = string.Empty;
            try
            {
				managersModel.RemoveByID(Id);
                msg = "Succesfull";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { msg = msg });
        }
        [HttpGet]
        public ActionResult ManagerList()
        {
			var model = managersModel.ReturnAll().Select(x => new ManagerVM
            {
                ManagerID = x.ManagersId,
                FullName = x.Fullname,
                PhoneNumber = x.Phonenumber
            }).ToList();


            return PartialView(model);
        }

        [HttpGet]
        public ActionResult CreateManager()
        {
            return PartialView();
        }
        [HttpPost]
		public ActionResult CreateManager(ManagerVM Model)
        {
            bool success = false;
            string msg = string.Empty;

            try
            {
				Managers managers = new Managers();
				managers.Fullname = Model.FullName;
				managers.Phonenumber = Model.PhoneNumber;
				managers.Password = Model.Password;

				managersModel.Add(managers);
                success = true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Json(new { success = success, msg = msg });
        }
    }
}
