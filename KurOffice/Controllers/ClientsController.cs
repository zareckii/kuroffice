﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using KurOffice.Models;
using KurOffice.ViewModel;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KurOffice.Controllers
{
	[Authorize]
	public class ClientsController : BaseController
    {
		private ClientsModel clientsModel = new ClientsModel();

        // GET: /<controller>/
        public IActionResult Index()
        {
			var model = clientsModel.ReturnAll().Select(x => new ClientVM
            {
                ClientID = x.ClientsId,
                FullName = x.Fullname,
                PhoneNumber = x.Phonenumber
            }).ToList();

			return View(model);
        }

		[HttpPost]
		public void AddClient(Clients item)
		{
			clientsModel.Add(item);
		}

		[HttpPost]
		public ActionResult RemoveClient(int Id)
		{
			string msg = string.Empty;
            try
			{
				clientsModel.RemoveByID(Id);
				msg = "Succesfull";
			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}
			return Json(new { msg = msg });
		}
        [HttpGet]
		public ActionResult ClientList()
		{
			var model = clientsModel.ReturnAll().Select(x => new ClientVM
            {
                ClientID = x.ClientsId,
                FullName = x.Fullname,
                PhoneNumber = x.Phonenumber
            }).ToList();

			return PartialView(model);
		}

        [HttpGet]
		public ActionResult CreateClient(){
			return PartialView();
		}
        [HttpPost]
		public ActionResult CreateClient(ClientVM Model)
        {
			bool success = false;
			string msg = string.Empty;

            try
			{
				Clients clients = new Clients();
                clients.Fullname = Model.FullName;
                clients.Phonenumber = Model.PhoneNumber;

                clientsModel.Add(clients);
				success = true;
			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}

			return Json(new {success = success, msg = msg});
        }
    }
}
