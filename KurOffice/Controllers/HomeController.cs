﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KurOffice.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using KurOffice.ViewModel;

namespace KurOffice.Controllers
{
	[Authorize]
	public class HomeController : BaseController
    {
        public IActionResult Index()
        {      
			OfficesModel officesModel = new OfficesModel();
			List<OfficeStatisticVM> model = null;
			if (User != null && User.Identity != null && User.Identity.Name != null)
			{
				if (User.Identity.Name == "Admin")
				{
					GlobalVeriables.connectionString = "Host=localhost;Database=KurOffices;Username=postgres;Password=1488";

                     model = officesModel.GetOfficeStatictic();
				}else
    				{
					GlobalVeriables.connectionString = "Host=localhost;Database=KurOffices;Username=manager;Password=1234";

                    model = officesModel.GetOfficeStatictic();
				}
			}
			return View(model);
		}
        
		//[HttpGet]
        //public ActionResult StatisticList()
        //{
        //    return PartialView(model);
        //}

    }
}
