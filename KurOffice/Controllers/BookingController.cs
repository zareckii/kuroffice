﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using KurOffice.Models;
using KurOffice.ViewModel;
using KurOffice.Controllers;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KurOffice
{
	[Authorize]
	public class BookingController : BaseController
    {
			
		BookingsModel bookingsModel = new BookingsModel();
		BookingsDateModel bookingsDate = new BookingsDateModel();
		ClientsModel clientsModel = new ClientsModel();
		InvoiceModel invoiceModel = new InvoiceModel();
		OfficesModel officesModel = new OfficesModel();
		ManagersModel managersModel = new ManagersModel();
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }


		public void CreateInvoice(Bookings bookings, DateTime dateTime, decimal orders)
		{
			if (bookingsModel.Return(bookings.BookingsId) != null)
			{
				invoiceModel.Add(new Invoice() { BookingsId = bookings.BookingdateId, Datetime = dateTime, Orders = orders });
			}
		}

		public ActionResult BookingList()
		{
			List<BookingVM> model = new List<BookingVM>();
            string msg = string.Empty;
			try
			{

                model = (from book in bookingsModel.ReturnAll()
                         join c in clientsModel.ReturnAll()
                            on book.ClientsId equals c.ClientsId
                         join Of in officesModel.ReturnAll()
                            on book.OfficesId equals Of.OfficesId
                         join bd in bookingsDate.ReturnAll()
                            on book.BookingdateId equals bd.BookingdateId
                         join mn in managersModel.ReturnAll()
				          on book.ManagersId equals mn.ManagersId into Yg
                            from y1 in Yg.DefaultIfEmpty()
                            
                         select new BookingVM
                         {
                             ClientName = c.Fullname,
                             ManagerName = y1 == null ? "" : y1.Fullname,
                             BookingName = book.Name,
                             StartDate = bd.Startdate,
                             CreatedDate = bd.Bookingdate1,
                             OfficeNumber = Of.Number,
                             Price = book.Price,
                             Term = bd.Terms,
                             BookingId = book.BookingsId
                         }).ToList();

				model.ForEach(x =>
				              x.isExpired = x.StartDate.AddMonths(x.Term.HasValue ? x.Term.Value : 0) < DateTime.Now
							 );

			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}


			return PartialView(model);
		}


        [HttpGet]
		public ActionResult AddOffice(int? Area = null, int? CountRoom = null, int? Floor= null, int? Number= null,DateTime? bookingStartDate = null){
			List<OfficeVM> model = new List<OfficeVM>();
            try
			{
				model = officesModel.getOfficeWithFilter(Area, CountRoom, Floor, Number,bookingStartDate).Select(x => new OfficeVM
                {
                    OfficesId = x.OfficesId,
                    Area = x.Area,
                    Countroom = x.Countroom,
                    Floor = x.Floor,
                    Kitchen = x.Kitchen,
                    Number = x.Number,
                    Restroom = x.Restroom,
                    Startprice = x.Startprice
                }).ToList();
			}
			catch (Exception ex)
			{

			}

			return PartialView(model);
		}

		public ActionResult BookingFilter(){
			return PartialView();
		}

		public ActionResult AddClient(){
			var model = clientsModel.ReturnAll().Select(x => new ClientVM
            {
                ClientID = x.ClientsId,
                FullName = x.Fullname,
                PhoneNumber = x.Phonenumber
            }).ToList();
			return PartialView(model);
		}

		public ActionResult AddBookingData()
        {
            return PartialView();
        }


		public Bookings GetBooking(string name)
		{
			return bookingsModel.Return(name);
		}
        

		public List<Invoice> GetAllInvoice()
        {
            return invoiceModel.ReturnAll();
        }

		public void RemoveBooking(Bookings item)
		{
			bookingsModel.Remove(item);
		}

		public void RemoveAllBooking()
        {
			bookingsModel.RemoveAll();
        }

		[HttpGet]
		public ActionResult CreateBooking()
		{
			return PartialView();
		}

        [HttpPost]
		public ActionResult SaveBooking(SaveBookingVM booking){

			bool success = true;
			bool isValid = true;
			string msg = string.Empty;

            if (booking != null)
			{
                if (booking.bookingDate < DateTime.Now)
				{
					msg = "Не коректная дата букинга";
					isValid = false;
				}
                if (booking.price <= 0)
				{
					msg = "Price не может быть меньше 0 или быть равен 0";
					isValid = false;
				}
                if (string.IsNullOrEmpty(booking.name))
				{
					msg = "Введите имя букинга";
					isValid = false;
    			}
				if (!string.IsNullOrEmpty(booking.name) && booking.name.Length < 6)
                {
                    msg = "Имя букинга должно быть длинее 6 символов";
					isValid = false;
                }
				if (isValid)
				{
					var bookingValidation = bookingsModel.Return(booking.name);
                    if (bookingValidation != null)
                    {
                        msg = "Букинг с таким именем уже существует";
                        isValid = false;
                    }
				}


			}else
			{
				msg = "Bed Request";
				isValid = false;
			}

			if (isValid)
			{
    			try
    			{
    				Bookingdate bookingdate = new Bookingdate();
    				bookingdate.Bookingdate1 = DateTime.Now;
    				bookingdate.Startdate = booking.bookingDate;
    				bookingdate.Terms = booking.term;
    				bookingsDate.Add(bookingdate);

    				var bookingD = bookingsDate.ReturnLast();

    				var ManagerId = managersModel.GetByName(User.Identity.Name);

    				Bookings bookings = new Bookings();
    				bookings.ManagersId = ManagerId.ManagersId;
    				bookings.OfficesId = booking.officeId;
    				bookings.ClientsId = booking.clientId;
    				bookings.Name = booking.name;
                    bookings.Price = booking.price;
    				bookings.BookingdateId = bookingD.BookingdateId;

    				bookingsModel.Add(bookings);

    			} catch (Exception ex) {
    				success = false;
    				msg = ex.Message;
    				Json(new { success = success, msg = msg });
                }
			}else
			{
				success = isValid;
			}

			return Json(new {success = success, msg = msg});
		}


    }
}
