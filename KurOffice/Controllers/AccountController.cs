﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using KurOffice.Models;
using KurOffice.ViewModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KurOffice.Controllers
{
	public class AccountController : BaseController
    {

		KurOfficesContext db;
		public AccountController()
        {
			db = new KurOfficesContext();
        }

		//[HttpGet]
		//public ActionResult Index()
		//{
		//	return View();
		//}

		[HttpGet]
        public IActionResult Login()
        {
            return View();
        }
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginModel model)
		{
			GlobalVeriables.connectionString = "Host=localhost;Database=KurOffices;Username=postgres;Password=1488"; // TODO: Strart Connection
			if (ModelState.IsValid)
			{

                if (model.Admin && model.Login == "Admin" && model.Password == "Admin")
				{
					try
                    {
						GlobalVeriables.connectionString = "Host=localhost;Database=KurOffices;Username=postgres;Password=1488"; //TODO: Dima syka roli dobav
                        await Authenticate(model.Login); // аутентификация

                    }
                    catch (Exception ex)
                    {
                        throw;
                    }


                    return RedirectToAction("Index", "Home");
				}

				Managers user = await db.Managers.FirstOrDefaultAsync(u => u.Fullname == model.Login && u.Password == model.Password);
				if (user != null)
				{
					try
					{
						GlobalVeriables.connectionString = "Host=localhost;Database=KurOffices;Username=manager;Password=1234";
						await Authenticate(model.Login); // аутентификация
						 //TODO: Dima syka roli dobav
					}
					catch (Exception ex)
					{
						throw;
					}


					return RedirectToAction("Index", "Home");
				}
				ModelState.AddModelError("", "Некорректные логин и(или) пароль");
			}
			return View(model);
		}

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}
