﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using KurOffice.Models;
using KurOffice.ViewModel;
using KurOffice.Controllers;
using Microsoft.AspNetCore.Authorization;

namespace KurOffice
{
    [Authorize]
    public class InvoiceController : BaseController
    {

                BookingsModel bookingsModel = new BookingsModel();
                BookingsDateModel bookingsDate = new BookingsDateModel();
                InvoiceModel invoiceModel = new InvoiceModel();
        
        [HttpGet]
        public ActionResult InvoiceList(int id){
            List<InvoiceVM> model = new List<InvoiceVM>();
            model = invoiceModel.GetBookingInvoices(id).Select(x => new InvoiceVM
            {
                InvioceId = x.InvioceId,
                Datetime = x.Datetime != null ? x.Datetime.ToShortDateString() : "",
                Orders = x.Orders
            }).ToList();

            decimal totalSum = 0;
                model.ForEach(x => totalSum += x.Orders);

            ViewBag.TotalSum = totalSum;
            ViewBag.BookingId = id;
            return PartialView(model);

        }
        [HttpGet]
        public ActionResult Index(int id)
        {
            ViewBag.BookingId = id;
            return PartialView();
        }
        [HttpGet]
        public ActionResult AddInvoice()
        {           
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddInvoice(InvoiceVM model)
        {

            string msg = string.Empty;
            bool success = true;

            try
            {
                Invoice invoice = new Invoice();
                invoice.BookingsId = model.BookingsId;
                invoice.Datetime = DateTime.Now;
                invoice.Orders = model.Orders;

                invoiceModel.Add(invoice);

            }
            catch (Exception ex)
            {
                success = false;
                msg = ex.Message;
            }

            return Json(new {success = success, msg = msg});
        }


    }
}
