﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KurOffice.Models;
using KurOffice.ViewModel;
using Microsoft.AspNetCore.Authorization;

namespace KurOffice.Controllers
{
	[Authorize]
	public class OfficeController : BaseController
    {
		private OfficesModel officesModel = new OfficesModel();

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

		public String AddOffice(Offices item)
		{
			officesModel.Add(item);
			return "";
		}

		public String test()
		{
			return officesModel.GetOfficeStatictic().First().office_id.ToString();
		}

		public void RemoveOffice(Offices item)
		{
			officesModel.Remove(item);
		}

		public List<Offices> SelectOffices(Offices offices)
		{
			return officesModel.SelectOffices(offices);
		}
        
        [HttpGet]
		public ActionResult OfficeList(){




			List<OfficeVM> model = officesModel.ReturnAll().Select(x => new OfficeVM {
				 OfficesId = x.OfficesId,
                 Area = x.Area,
                 Countroom = x.Countroom,
                 Floor = x.Floor,
                 Kitchen = x.Kitchen,
                 Number = x.Number,
                 Restroom = x.Restroom,
                 Startprice = x.Startprice
			}).ToList();
			return PartialView(model);
		}

        [HttpGet]
		public ActionResult AddOffice(){
			return PartialView();
		}

		[HttpPost]
        public ActionResult AddOffice(OfficeVM model)
        {
			bool success = false;
			string msg = string.Empty;

            try
			{
				Offices offices = new Offices();
				offices.Area = model.Area;
				offices.Floor = model.Floor;
				offices.Countroom = model.Countroom;
				offices.Kitchen = model.Kitchen;
				offices.Number = model.Number;
				offices.Restroom = model.Restroom;
				offices.Startprice = model.Startprice;
				officesModel.Add(offices);
				success = true;
			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}

			return Json(new {success = success, msg= msg});
        }

        [HttpPost]
		public ActionResult RemoveOffice(int Id){
			string msg = string.Empty;

            try
			{
				officesModel.RemoveById(Id);
			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}


			return Json(new { msg = msg });
		}

    }
}
