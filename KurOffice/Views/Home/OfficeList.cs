#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KurOffice.Views.Home
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


[System.CodeDom.Compiler.GeneratedCodeAttribute("RazorTemplatePreprocessor", "2.6.0.0")]
public partial class OfficeList : OfficeListBase
{

#line hidden

#line 1 "OfficeList.cshtml"
public List<KurOffice.ViewModel.OfficeVM> Model { get; set; }

#line default
#line hidden


public override void Execute()
{
WriteLiteral("    <table");

WriteLiteral(" class=\"table\"");

WriteLiteral(" id=\"officeList\"");

WriteLiteral(@">
        <thead>
            <tr>
                <th>Office ID</th>
                <th>Area</th>
                <th>Countroom</th>
                <th>Floor</th>
                <th>Kitchen</th>
                <th>Number</th>
                <th>Restroom</th>
                <th>Startprice</th>
                <th>Managment</th>
            </tr>
        </thead>
        <tbody>
");


#line 17 "OfficeList.cshtml"
            

#line default
#line hidden

#line 17 "OfficeList.cshtml"
             foreach (var item in Model)
            {


#line default
#line hidden
WriteLiteral("            <tr>\r\n                <td><a");

WriteLiteral(" href=\"#\"");

WriteLiteral(">");


#line 20 "OfficeList.cshtml"
                           Write(item.OfficesId);


#line default
#line hidden
WriteLiteral("</a></td>\r\n                <td>");


#line 21 "OfficeList.cshtml"
               Write(item.Area);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 22 "OfficeList.cshtml"
               Write(item.Countroom);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 23 "OfficeList.cshtml"
               Write(item.Floor);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 24 "OfficeList.cshtml"
               Write(item.Kitchen);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 25 "OfficeList.cshtml"
               Write(item.Number);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 26 "OfficeList.cshtml"
               Write(item.Restroom);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>");


#line 27 "OfficeList.cshtml"
               Write(item.Startprice);


#line default
#line hidden
WriteLiteral("</td>\r\n                <td>\r\n                    <input");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-defult deleteOffice\"");

WriteLiteral(" value=\"Delete\"");

WriteLiteral(" data-id=\"");


#line 29 "OfficeList.cshtml"
                                                                                                Write(item.OfficesId);


#line default
#line hidden
WriteLiteral("\"");

WriteLiteral(" />\r\n                </td>\r\n            </tr>\r\n");


#line 32 "OfficeList.cshtml"
            }


#line default
#line hidden
WriteLiteral(@"        </tbody>
    </table>
    <script>

    $('#officeList').DataTable().on(""draw.dt"", function () {
      $('.deleteOffice').off().on('click', function() {
              var OfficeId = $(this).data('id');
              $.ajax({
                  url: '../Office/RemoveOffice',
                  type: 'Post',
                  data: { Id: OfficeId },
                  success: function(data) {
                      updateOfficeList();
                  }
              });
        });
    });


    $('.deleteOffice').off().on('click', function() {
            var OfficeId = $(this).data('id');
            $.ajax({
                url: '../Office/RemoveOffice',
                type: 'Post',
                data: { Id: OfficeId },
                success: function(data) {
                  alert(data.msg);
                    updateOfficeList();
                }
            });
});

var updateOfficeList = function() {
            $.ajax({
                url: '../Office/OfficeList',
                type: 'Get',
                data: {},
                success: function(data) {
                    $('#officeData').html(data);
                }
            });
}</script>
");

}
}

// NOTE: this is the default generated helper class. You may choose to extract it to a separate file 
// in order to customize it or share it between multiple templates, and specify the template's base 
// class via the @inherits directive.
public abstract class OfficeListBase
{

		// This field is OPTIONAL, but used by the default implementation of Generate, Write, WriteAttribute and WriteLiteral
		//
		System.IO.TextWriter __razor_writer;

		// This method is OPTIONAL
		//
		/// <summary>Executes the template and returns the output as a string.</summary>
		/// <returns>The template output.</returns>
		public string GenerateString ()
		{
			using (var sw = new System.IO.StringWriter ()) {
				Generate (sw);
				return sw.ToString ();
			}
		}

		// This method is OPTIONAL, you may choose to implement Write and WriteLiteral without use of __razor_writer
		// and provide another means of invoking Execute.
		//
		/// <summary>Executes the template, writing to the provided text writer.</summary>
		/// <param name="writer">The TextWriter to which to write the template output.</param>
		public void Generate (System.IO.TextWriter writer)
		{
			__razor_writer = writer;
			Execute ();
			__razor_writer = null;
		}

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>Writes a literal value to the template output without HTML escaping it.</summary>
		/// <param name="value">The literal value.</param>
		protected void WriteLiteral (string value)
		{
			__razor_writer.Write (value);
		}

		// This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
		//
		/// <summary>Writes a literal value to the TextWriter without HTML escaping it.</summary>
		/// <param name="writer">The TextWriter to which to write the literal.</param>
		/// <param name="value">The literal value.</param>
		protected static void WriteLiteralTo (System.IO.TextWriter writer, string value)
		{
			writer.Write (value);
		}

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>Writes a value to the template output, HTML escaping it if necessary.</summary>
		/// <param name="value">The value.</param>
		/// <remarks>The value may be a Action<System.IO.TextWriter>, as returned by Razor helpers.</remarks>
		protected void Write (object value)
		{
			WriteTo (__razor_writer, value);
		}

		// This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
		//
		/// <summary>Writes an object value to the TextWriter, HTML escaping it if necessary.</summary>
		/// <param name="writer">The TextWriter to which to write the value.</param>
		/// <param name="value">The value.</param>
		/// <remarks>The value may be a Action<System.IO.TextWriter>, as returned by Razor helpers.</remarks>
		protected static void WriteTo (System.IO.TextWriter writer, object value)
		{
			if (value == null)
				return;

			var write = value as Action<System.IO.TextWriter>;
			if (write != null) {
				write (writer);
				return;
			}

			//NOTE: a more sophisticated implementation would write safe and pre-escaped values directly to the
			//instead of double-escaping. See System.Web.IHtmlString in ASP.NET 4.0 for an example of this.
			writer.Write(System.Net.WebUtility.HtmlEncode (value.ToString ()));
		}

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>
		/// Conditionally writes an attribute to the template output.
		/// </summary>
		/// <param name="name">The name of the attribute.</param>
		/// <param name="prefix">The prefix of the attribute.</param>
		/// <param name="suffix">The suffix of the attribute.</param>
		/// <param name="values">Attribute values, each specifying a prefix, value and whether it's a literal.</param>
		protected void WriteAttribute (string name, string prefix, string suffix, params Tuple<string,object,bool>[] values)
		{
			WriteAttributeTo (__razor_writer, name, prefix, suffix, values);
		}

		// This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
		//
		/// <summary>
		/// Conditionally writes an attribute to a TextWriter.
		/// </summary>
		/// <param name="writer">The TextWriter to which to write the attribute.</param>
		/// <param name="name">The name of the attribute.</param>
		/// <param name="prefix">The prefix of the attribute.</param>
		/// <param name="suffix">The suffix of the attribute.</param>
		/// <param name="values">Attribute values, each specifying a prefix, value and whether it's a literal.</param>
		///<remarks>Used by Razor helpers to write attributes.</remarks>
		protected static void WriteAttributeTo (System.IO.TextWriter writer, string name, string prefix, string suffix, params Tuple<string,object,bool>[] values)
		{
			// this is based on System.Web.WebPages.WebPageExecutingBase
			// Copyright (c) Microsoft Open Technologies, Inc.
			// Licensed under the Apache License, Version 2.0
			if (values.Length == 0) {
				// Explicitly empty attribute, so write the prefix and suffix
				writer.Write (prefix);
				writer.Write (suffix);
				return;
			}

			bool first = true;
			bool wroteSomething = false;

			for (int i = 0; i < values.Length; i++) {
				Tuple<string,object,bool> attrVal = values [i];
				string attPrefix = attrVal.Item1;
				object value = attrVal.Item2;
				bool isLiteral = attrVal.Item3;

				if (value == null) {
					// Nothing to write
					continue;
				}

				// The special cases here are that the value we're writing might already be a string, or that the 
				// value might be a bool. If the value is the bool 'true' we want to write the attribute name instead
				// of the string 'true'. If the value is the bool 'false' we don't want to write anything.
				//
				// Otherwise the value is another object (perhaps an IHtmlString), and we'll ask it to format itself.
				string stringValue;
				bool? boolValue = value as bool?;
				if (boolValue == true) {
					stringValue = name;
				} else if (boolValue == false) {
					continue;
				} else {
					stringValue = value as string;
				}

				if (first) {
					writer.Write (prefix);
					first = false;
				} else {
					writer.Write (attPrefix);
				}

				if (isLiteral) {
					writer.Write (stringValue ?? value);
				} else {
					WriteTo (writer, stringValue ?? value);
				}
				wroteSomething = true;
			}
			if (wroteSomething) {
				writer.Write (suffix);
			}
		}
		// This method is REQUIRED. The generated Razor subclass will override it with the generated code.
		//
		///<summary>Executes the template, writing output to the Write and WriteLiteral methods.</summary>.
		///<remarks>Not intended to be called directly. Call the Generate method instead.</remarks>
		public abstract void Execute ();

}
}
#pragma warning restore 1591
