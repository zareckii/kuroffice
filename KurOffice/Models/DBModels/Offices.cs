﻿using System;
using System.Collections.Generic;

namespace KurOffice
{
    public partial class Offices
    {
        public Offices()
        {
            Bookings = new HashSet<Bookings>();
        }

        public int OfficesId { get; set; }
        public int? Area { get; set; }
        public int? Countroom { get; set; }
        public int? Floor { get; set; }
        public int? Number { get; set; }
        public bool? Kitchen { get; set; }
        public bool? Restroom { get; set; }
        public decimal Startprice { get; set; }

        public ICollection<Bookings> Bookings { get; set; }
    }
}
