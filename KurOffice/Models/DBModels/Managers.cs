﻿using System;
using System.Collections.Generic;

namespace KurOffice
{
    public partial class Managers
    {
        public Managers()
        {
            Bookings = new HashSet<Bookings>();
        }

        public int ManagersId { get; set; }
        public string Fullname { get; set; }
		public string Password { get; set; }
        public string Phonenumber { get; set; }

        public ICollection<Bookings> Bookings { get; set; }
    }
}
