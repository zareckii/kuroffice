﻿using System;
namespace KurOffice
{
    public class BookingsList
    {
		public string ClientFullName { get; set; }
        public string ManagerFullName { get; set; }
        public string Number { get; set; }
        public DateTime StartDate { get; set; }
        public int Terms { get; set; }
        public DateTime Bookingdate { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
    }
}
