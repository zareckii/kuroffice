﻿using System;
using System.Collections.Generic;

namespace KurOffice
{
    public partial class Invoice
    {
        public int InvioceId { get; set; }
        public int? BookingsId { get; set; }
        public DateTime Datetime { get; set; }
        public decimal Orders { get; set; }

        public Bookings Bookings { get; set; }
    }
}
