﻿using System;
using System.Collections.Generic;

namespace KurOffice
{
    public partial class Bookings
    {
        public Bookings()
        {
            Invoice = new HashSet<Invoice>();
        }

        public int BookingsId { get; set; }
        public int? ManagersId { get; set; }
        public int? ClientsId { get; set; }
        public int? OfficesId { get; set; }
        public int? BookingdateId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public Bookingdate Bookingdate { get; set; }
        public Clients Clients { get; set; }
        public Managers Managers { get; set; }
        public Offices Offices { get; set; }
        public ICollection<Invoice> Invoice { get; set; }
    }
}
