﻿using System.Collections.Generic;

namespace KurOffice
{
	public partial class Clients
    {
        public Clients()
        {
            Bookings = new HashSet<Bookings>();
        }

        public int ClientsId { get; set; }
        public string Fullname { get; set; }
        public string Phonenumber { get; set; }

        public ICollection<Bookings> Bookings { get; set; }
    }
}
