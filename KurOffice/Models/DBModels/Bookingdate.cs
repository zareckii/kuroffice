﻿using System;
using System.Collections.Generic;

namespace KurOffice
{
    public partial class Bookingdate
    {
        public int BookingdateId { get; set; }
        public DateTime Bookingdate1 { get; set; }
        public DateTime Startdate { get; set; }
        public int? Terms { get; set; }

        public Bookings Bookings { get; set; }
    }
}
