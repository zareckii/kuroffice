﻿using System;
using KurOffice.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KurOffice
{
    public partial class KurOfficesContext : DbContext
    {
        public virtual DbSet<Bookingdate> Bookingdate { get; set; }
        public virtual DbSet<Bookings> Bookings { get; set; }
        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Managers> Managers { get; set; }
        public virtual DbSet<Offices> Offices { get; set; }
        
       
		string dbConnection = String.Empty;

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
				optionsBuilder.UseNpgsql(GlobalVeriables.connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bookingdate>(entity =>
            {
                entity.ToTable("bookingdate");

                entity.Property(e => e.BookingdateId).HasColumnName("bookingdate_id");

                entity.Property(e => e.Bookingdate1)
                    .HasColumnName("bookingdate")
                    .HasColumnType("date");

                entity.Property(e => e.Startdate)
                    .HasColumnName("startdate")
                    .HasColumnType("date");

                entity.Property(e => e.Terms)
                    .HasColumnName("terms")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<Bookings>(entity =>
            {
                entity.ToTable("bookings");

                entity.HasIndex(e => e.BookingdateId)
                    .HasName("bookings_bookingdate_id_key")
                    .IsUnique();

                entity.Property(e => e.BookingsId).HasColumnName("bookings_id");

                entity.Property(e => e.BookingdateId).HasColumnName("bookingdate_id");

                entity.Property(e => e.ClientsId).HasColumnName("clients_id");

                entity.Property(e => e.ManagersId).HasColumnName("managers_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying(10)")
                    .HasDefaultValueSql("'DefaultName'::character varying");

                entity.Property(e => e.OfficesId).HasColumnName("offices_id");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");

                entity.HasOne(d => d.Bookingdate)
                    .WithOne(p => p.Bookings)
                    .HasForeignKey<Bookings>(d => d.BookingdateId)
                    .HasConstraintName("bookings_bookingdate_id_fkey");

                entity.HasOne(d => d.Clients)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.ClientsId)
                    .HasConstraintName("bookings_clients_id_fkey");

                entity.HasOne(d => d.Managers)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.ManagersId)
                    .HasConstraintName("bookings_managers_id_fkey");

                entity.HasOne(d => d.Offices)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.OfficesId)
                    .HasConstraintName("bookings_offices_id_fkey");
            });

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.ToTable("clients");

                entity.HasIndex(e => e.Phonenumber)
                    .HasName("clients_phonenumber_key")
                    .IsUnique();

                entity.Property(e => e.ClientsId).HasColumnName("clients_id");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasColumnType("character varying(30)")
                    .HasDefaultValueSql("'Client'::character varying");

                entity.Property(e => e.Phonenumber)
                    .IsRequired()
                    .HasColumnName("phonenumber")
                    .HasColumnType("character varying(15)");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.InvioceId);

                entity.ToTable("invoice");

                entity.Property(e => e.InvioceId).HasColumnName("invioce_id");

                entity.Property(e => e.BookingsId).HasColumnName("bookings_id");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("date");

                entity.Property(e => e.Orders)
                    .HasColumnName("orders")
                    .HasColumnType("money");

                entity.HasOne(d => d.Bookings)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.BookingsId)
                    .HasConstraintName("invoice_bookings_id_fkey");
            });

            modelBuilder.Entity<Managers>(entity =>
            {
                entity.ToTable("managers");

                entity.HasIndex(e => e.Phonenumber)
                    .HasName("managers_phonenumber_key")
                    .IsUnique();

                entity.Property(e => e.ManagersId).HasColumnName("managers_id");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasColumnType("character varying(30)")
                    .HasDefaultValueSql("'Manager'::character varying");

				entity.Property(e => e.Password)
				      .HasColumnName("password")
                    .HasColumnType("character varying(30)")
                    .HasDefaultValueSql("'1111'::character varying");


                entity.Property(e => e.Phonenumber)
                    .IsRequired()
                    .HasColumnName("phonenumber")
                    .HasColumnType("character varying(15)");
            });

            modelBuilder.Entity<Offices>(entity =>
            {
                entity.ToTable("offices");

                entity.Property(e => e.OfficesId).HasColumnName("offices_id");

                entity.Property(e => e.Area)
                    .HasColumnName("area")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Countroom)
                    .HasColumnName("countroom")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Floor)
                    .HasColumnName("floor")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Kitchen)
                    .HasColumnName("kitchen")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Restroom)
                    .HasColumnName("restroom")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Startprice)
                    .HasColumnName("startprice")
                    .HasColumnType("money");
            });
        }
    }
}
