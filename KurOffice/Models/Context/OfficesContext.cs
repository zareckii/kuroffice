﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KurOffice
{
    public partial class OfficesContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql(@"Host=localhost;Database=Offices;Username=postgres;Password=1488");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {}
    }
}




//string const UserConnection = @"Host=localhost;Database=KurOffices;Username=postgres;Password=1488";
//string const ManagerConnection = @"Host=localhost;Database=KurOffices;Username=postgres;Password=1488";

//string dbConnection = String.Empty;

//public KurOfficesContext(bool manager)
//{
//    if (manager)
//    {
//        dbConnection = ManagerConnection;
//    }
//    else
//    {
//        dbConnection = UserConnection;
//    }
//}