﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace KurOffice.Models
{
    public class BookingsDateModel: ICRUD<Bookingdate>
    {
		private KurOfficesContext db;

		public BookingsDateModel()
        {
            db = new KurOfficesContext();
        }

		public void Add(Bookingdate item)
        {
			db.Database.ExecuteSqlCommand("INSERT INTO bookingdate (Bookingdate, StartDate, terms) VALUES ('" +item.Bookingdate1.Year+"-"+item.Bookingdate1.Month+"-"+item.Bookingdate1.Day + "', '" + item.Startdate.Year + "-" + item.Startdate.Month + "-" + item.Startdate.Day + "',"+item.Terms+")");
			db.Bookingdate.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void AddList(List<Bookingdate> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO bookingdate (Bookingdate, StartDate, terms) VALUES ('" + index.Bookingdate1.Year + "-" + index.Bookingdate1.Month + "-" + index.Bookingdate1.Day + "', '" + index.Startdate.Year + "-" + index.Startdate.Month + "-" + index.Startdate.Day + "'," + index.Terms + ")");
				db.Bookingdate.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

		public void Remove(Bookingdate item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM bookingdate CASCADE WHERE bookingdate_id = " + item.BookingdateId.ToString());
			db.Bookingdate.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM bookingdate CASCADE");
			db.Bookingdate.FromSql("COMMIT");
            db.SaveChanges();
        }

		public Bookingdate Return(int id)
        {
			return (Bookingdate)db.Bookingdate.FromSql("SELECT * FROM bookingdate WHERE bookingdate_id = " + id);
        }

		public List<Bookingdate> ReturnAll()
        {
			return db.Bookingdate.FromSql("SELECT * FROM bookingdate").ToList();
        }

		public Bookingdate ReturnLast()
        {
			return db.Bookingdate.FromSql("SELECT * FROM bookingdate WHERE bookingdate_id = (SELECT max(bookingdate_id) FROM bookingdate)").FirstOrDefault( );
        }

		public Bookingdate Update(Bookingdate item)
        {
            throw new NotImplementedException();
        }
    }
}
