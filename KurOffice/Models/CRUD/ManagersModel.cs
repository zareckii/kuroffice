﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace KurOffice.Models
{
	public class ManagersModel : ICRUD<Managers>
	{
		private KurOfficesContext db;

		public ManagersModel()
		{
			db = new KurOfficesContext();
		}

		public void Add(Managers item)
		{
			db.Database.ExecuteSqlCommand("INSERT INTO Managers (fullName, phoneNumber,password) VALUES ('"+item.Fullname+"', '" +item.Phonenumber +"', '" + item.Password + "')");
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
		}

        public void AddList(List<Managers> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO Managers (fullName, phoneNumber) VALUES ('" + index.Fullname + "', '" +index.Phonenumber + "')");
                db.Managers.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

        public void Remove(Managers item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM managers CASCADE WHERE managers_id = " + item.ManagersId.ToString());
			db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void RemoveByID(int ManagerID)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM managers CASCADE WHERE managers_id = " + ManagerID.ToString());
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM managers CASCADE");
			db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

        public Managers Return(int id)
        {
            return (Managers)db.Managers.FromSql("SELECT * FROM Managers WHERE managers_id = " + id);
        }
        
        public List<Managers> ReturnAll()
        {
            return db.Managers.FromSql("SELECT * FROM Managers").ToList();
        }

		public Managers GetByName(string Name)
        {
			return db.Managers.FromSql("select * from Managers where fullname = '"+ Name +"'").FirstOrDefault();
        }


        public Managers ReturnLast()
        {
            return (Managers)db.Managers.FromSql("SELECT * FROM managers WHERE managers_id = (SELECT max(managers_id) FROM managers)");
        }

        public Managers Update(Managers item)
        {
            throw new NotImplementedException();
        }
    }
}
