﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace KurOffice.Models
{
    public class ClientsModel: ICRUD<Clients>
    {
		private KurOfficesContext db;

		public ClientsModel()
        {
            db = new KurOfficesContext();
        }

		public void Add(Clients item)
        {
			db.Database.ExecuteSqlCommand("INSERT INTO clients (fullName, phoneNumber) VALUES ('" + item.Fullname + "', '" + item.Phonenumber + "')");
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void AddList(List<Clients> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO clients (fullName, phoneNumber) VALUES ('" + index.Fullname + "', '" + index.Phonenumber + "')");
                db.Managers.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

		public void Remove(Clients item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM clients CASCADE WHERE clients_id = " + item.ClientsId.ToString());
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void RemoveByID(int ClientID)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM clients CASCADE WHERE clients_id = " + ClientID.ToString());
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM clients CASCADE");
            db.Managers.FromSql("COMMIT");
            db.SaveChanges();
        }

		public Clients Return(int id)
        {
			return (Clients)db.Managers.FromSql("SELECT * FROM clients WHERE clients_id = " + id);
        }

		public Clients Return(string fullName)
		{
			return (Clients)db.Managers.FromSql("SELECT * FROM clients WHERE fullName = " + fullName);
		}

		public List<Clients> ReturnAll()
        {
			return db.Clients.FromSql("SELECT * FROM clients").ToList();
        }

		public Clients ReturnLast()
        {
			return (Clients)db.Managers.FromSql("SELECT * FROM clients WHERE clients_id = (SELECT max(clients_id) FROM clients)");
        }

		public Clients Update(Clients item)
        {
            throw new NotImplementedException();
        }
    }
}
