﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using KurOffice.ViewModel;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using NpgsqlTypes;

namespace KurOffice.Models
{
    public class OfficesModel : ICRUD<Offices>
    {
		private KurOfficesContext db;

		public OfficesModel()
        {
            db = new KurOfficesContext();
        }

		public void Add(Offices item)
        {
			db.Database.ExecuteSqlCommand("INSERT INTO offices (area, countRoom, floor, number, restroom, kitchen, startPrice) VALUES ("+item.Area+", "+item.Countroom+", "+item.Floor+", " + item.Number + ", " + item.Restroom + ", " + item.Kitchen + ", " + item.Startprice + ")");
			db.Offices.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void AddList(List<Offices> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO offices (area, countRoom, floor, number, restroom, kitchen, startPrice) VALUES (" + index.Area + ", " + index.Countroom + ", " + index.Floor + ", " + index.Number + ", " + index.Restroom + ", " + index.Kitchen + ", " + index.Startprice + ")");
				db.Offices.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

		public List<Offices> SelectOffices(Offices item)
		{
			if (item.Startprice != 0)
			{
				return db.Offices.FromSql("SELECT * FROM offices WHERE kitchen = " + item.Kitchen.ToString() + " and restroom = " + item.Restroom.ToString() + " and floor = " + item.Floor.ToString() + "and startprice:: numeric = " + item.Startprice.ToString()).ToList() as List<Offices>;
			}
			else
			{
				return db.Offices.FromSql("SELECT * FROM offices WHERE kitchen = " + item.Kitchen.ToString() + " and restroom = " + item.Restroom.ToString() + " and floor = " + item.Floor.ToString()).ToList() as List<Offices>;
			}
		}

		public void Remove(Offices item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM offices CASCADE WHERE offices_id = " + item.OfficesId.ToString());
			db.Offices.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void RemoveById(int Id)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM offices CASCADE WHERE offices_id = " + Id.ToString());
            db.Offices.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM offices CASCADE");
			db.Offices.FromSql("COMMIT");
            db.SaveChanges();
        }

		public Offices Return(int id)
        {
			return (Offices)db.Offices.FromSql("SELECT * FROM offices WHERE offices_id = " + id);
        }

		public List<Offices> ReturnAll()
        {
			return db.Offices.FromSql("SELECT * FROM offices").ToList();
        }

		public List<Offices> ReturnOfficeForBooking()
		{
			return db.Offices.FromSql(
				@"select o.* from offices as o
                    left join bookings as b on o.offices_id = b.offices_id
                    where b.bookings_id isnull

                union select ofi.*
                    from offices as ofi
                    join bookings as b on ofi.offices_id = b.offices_id
                    join bookingdate as bd on bd.bookingdate_id = b.bookings_id
                  where bd.startdate + interval '1 mon' * bd.terms < current_date"
			).ToList();

		}


		public List<OfficeVM> getOfficeWithFilter(int? Area, int? CountRoom, int? Floor, int? Number, DateTime? Date)
		{
			List<OfficeVM> officeList = new List<OfficeVM>();
			string constr = GlobalVeriables.connectionString;
			NpgsqlConnection connection = new NpgsqlConnection(constr);
			connection.Open();
			string AreaString = Area.HasValue ? Area.Value.ToString() : "null";
			string CountRoomString = CountRoom.HasValue ? CountRoom.Value.ToString() : "null";
			string FloorString = Floor.HasValue ? Floor.Value.ToString() : "null";
			string NumberString = Number.HasValue ? Number.Value.ToString() : "null";
			string DateString = Date.HasValue ? "'" + Date.Value.ToShortDateString()  + "'" : "null";
			var command = new NpgsqlCommand(@"select * from GetOfficesByFilter("
										  +
			                               AreaString
										  + "," +
			                              CountRoomString
										  + "," +
			                              FloorString
										  + "," +
			                              NumberString
										  + "," +
			                              DateString
										  + ")", connection);
			using (var reader = command.ExecuteReader())
			{
				for (int i = 0; reader.Read(); i++)
				{
					OfficeVM office = new OfficeVM();
					office.OfficesId = reader.GetInt32(0);
					office.Area = reader.GetInt32(1);
					office.Countroom = reader.GetInt32(2);
					office.Floor = reader.GetInt32(3);
					office.Number = reader.GetInt32(4);
					office.Kitchen = reader.GetBoolean(5);
					office.Restroom = reader.GetBoolean(6);
					office.Startprice = reader.GetDecimal(7);
					officeList.Add(office);
				}
			}
			connection.Close();
			return officeList;
		}

		public List<OfficeStatisticVM> GetOfficeStatictic()
		{
			List<OfficeStatisticVM> officeStatistics = new List<OfficeStatisticVM>();
			string constr = GlobalVeriables.connectionString;
			NpgsqlConnection connection = new NpgsqlConnection(constr);
			connection.Open();
			var command = new NpgsqlCommand(@"select distinct  bk.offices_id, bk.price as price, of.number,
        rank() over (order by count(bk.clients_id)) as clients_count,
        rank() over (order by avg(bd.terms)) as avg_terms
            from offices as of
                inner join bookings as bk on of.offices_id = bk.offices_id
                inner join clients as cl on bk.clients_id = cl.clients_id
                inner join bookingdate as bd on bk.bookingdate_id = bd.bookingdate_id
        group by bk.offices_id, bk.price, of.number", connection);
			using (var reder = command.ExecuteReader())
			{
				for (int i = 0; reder.Read(); i++)
				{     
					var ss = new OfficeStatisticVM();
					ss.office_id = reder.GetInt32(0);
					ss.Price = reder.GetDecimal(1);
					ss.OfficeNumber = reder.GetInt32(2);
					ss.ClientCount = reder.GetInt32(3);
					ss.AvgTerms = reder.GetInt32(4);
				    officeStatistics.Add(ss);
				}
			}
			connection.Close();
			return officeStatistics;
		}

		public Offices ReturnLast()
        {
			return (Offices)db.Offices.FromSql("SELECT * FROM offices WHERE offices_id = (SELECT max(offices_id) FROM offices)");
        }

		public Offices Update(Offices item)
        {
            throw new NotImplementedException();
        }
    }
}
