﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using KurOffice.ViewModel;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using NpgsqlTypes;

namespace KurOffice.Models
{
    public class InvoiceModel:ICRUD<Invoice>
    {
		private KurOfficesContext db;

		public InvoiceModel()
        {
            db = new KurOfficesContext();
        }

		public void Add(Invoice item)
        {
			db.Database.ExecuteSqlCommand("INSERT INTO invoice (bookings_id, datetime, orders) VALUES ("+item.BookingsId+",'" + item.Datetime.Year + "-" + item.Datetime.Month + "-" + item.Datetime.Day + "',"+item.Orders+")");
			db.Invoice.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void AddList(List<Invoice> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO invoice (bookings_id, datetime, orders) VALUES (" + index.BookingsId + "', '" + index.Datetime.Year + "-" + index.Datetime.Month + "-" + index.Datetime.Day + "', " + index.Orders + ")");
				db.Invoice.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

		public void Remove(Invoice item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM invoice CASCADE WHERE invoice_id = " + item.InvioceId.ToString());
			db.Invoice.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM invoice CASCADE");
			db.Invoice.FromSql("COMMIT");
            db.SaveChanges();
        }

		public decimal SumAllInvoice(int id)
		{
			decimal sum = 0;
            string constr = GlobalVeriables.connectionString;
            NpgsqlConnection connection = new NpgsqlConnection(constr);
            connection.Open();
			var command = new NpgsqlCommand(@"select sum(orders) from invoice where bookings_id = "+ id.ToString(), connection);
            using (var reader = command.ExecuteReader())
            {
                for (int i = 0; reader.Read(); i++)
                {
					sum = reader.GetDecimal(0);
                }
            }
                connection.Close();
			return sum; 
		}

		public Invoice Return(int id)
        {
			return (Invoice)db.Invoice.FromSql("SELECT * FROM invoice WHERE invoice_id = " + id);
        }

		public List<Invoice> ReturnAll()
        {
			return (List<Invoice>)db.Invoice.FromSql("SELECT * FROM invoice");
        }

		public List<Invoice> GetBookingInvoices(int bookings_id)
		{
			return db.Invoice.FromSql("SELECT * FROM invoice WHERE bookings_id=" + bookings_id).ToList();
		}

		public Invoice ReturnLast()
        {
			return (Invoice)db.Invoice.FromSql("SELECT * FROM invoice WHERE invoice_id = (SELECT max(invoice_id) FROM invoice)");
        }

		public Invoice Update(Invoice item)
        {
            throw new NotImplementedException();
        }

      
    }
}
