﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace KurOffice.Models
{
    public class BookingsModel: ICRUD<Bookings>
    {
		private KurOfficesContext db;

		public BookingsModel()
        {
			db = new KurOfficesContext();
        }

		public void Add(Bookings item)
        {
			db.Database.ExecuteSqlCommand("INSERT INTO bookings (name, price, managers_id, clients_id, offices_id, bookingdate_id )VALUES ('" + item.Name + "', " + item.Price + ", "+item.ManagersId+", "+item.ClientsId+", "+item.OfficesId+", "+item.BookingdateId+")");
			db.Bookings.FromSql("COMMIT");
            db.SaveChanges();
        }

		public void AddList(List<Bookings> array)
        {
            foreach (var index in array)
            {
				db.Database.ExecuteSqlCommand("INSERT INTO bookings (name, price, managers_id, clients_id, offices_id, bookingdate_id )VALUES ('" + index.Name + "', " + index.Price + ", " + index.ManagersId + ", " + index.ClientsId + ", " + index.OfficesId + ", " + index.BookingdateId + ")");
				db.Bookings.FromSql("COMMIT");
                db.SaveChanges();
            }

        }

		public void Remove(Bookings item)
        {
			db.Database.ExecuteSqlCommand("DELETE FROM bookings CASCADE WHERE bookings_id = " + item.BookingsId.ToString());
			db.Bookings.FromSql("COMMIT");
            db.SaveChanges();
        }

        public void RemoveAll()
        {
			db.Database.ExecuteSqlCommand("DELETE FROM bookings CASCADE");
			db.Bookings.FromSql("COMMIT");
            db.SaveChanges();
        }

		public Bookings Return(int id)
        {
			return (Bookings)db.Bookings.FromSql("SELECT * FROM bookings WHERE bookings_id = " + id);
        }

		public Bookings Return(string name)
        {
			return db.Bookings.FromSql("SELECT * FROM bookings WHERE name = '" + name + "'").FirstOrDefault();
        }

		public List<Bookings> ReturnAll()
        {
			return db.Bookings.FromSql("SELECT * FROM bookings").ToList();
        }


		//public List<BookingsList> ReturnBookingList()
   //     {
			//return db.BookingList.FromSql(
			//@" select  c.FullName as ClientFullName
      //               , m.FullName as ManagerFullName
      //               , o.Number
      //               , bd.StartDate
      //               , bd.Terms
      //               , bd.Bookingdate
      //               , b.Price
      //               , b.Name
      //          from Bookings as b
      //          join Clients as c on c.Clients_Id = b.Clients_Id
      //          join Managers as m on m.Managers_Id = b.Managers_Id
      //          join Offices as o on o.Offices_Id = b.Offices_Id
      //          join BookingDate bd on bd.Bookingdate_Id = b.BookingDate_Id"
		    //).ToList();
        //}

		public Bookings ReturnLast()
        {
			return (Bookings)db.Bookings.FromSql("SELECT * FROM bookings WHERE bookings_id = (SELECT max(bookings_id) FROM bookings)");
        }

		public Bookings Update(Bookings item)
        {
            throw new NotImplementedException();
        }
    }
}
