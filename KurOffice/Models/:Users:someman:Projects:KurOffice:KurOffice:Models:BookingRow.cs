﻿using System;
namespace KurOffice
{
    public class BookingRow    {
		public String Name { get; set; }
		public double Price { get; set; }
		public int Managersid { get; set; }
		public int clientsid { get; set; }
		public int officesid { get; set; }
		public int BookingsDateid { get; set; }
    }
}
