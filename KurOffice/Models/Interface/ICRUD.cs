﻿using System.Collections.Generic;

namespace KurOffice
{
    public interface ICRUD<T>
    {
        void Add(T item);
        void AddList(List<T> array);
        void Remove(T item);
        void RemoveAll();
        T Update(T item);
        T Return(int id);
        List<T> ReturnAll();
        T ReturnLast();
    }
}
